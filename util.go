package inquirer

import (
	"strconv"

	"github.com/fatih/color"
)

var yellow = color.New(color.FgYellow).SprintFunc()
var gray = color.New(color.FgWhite).SprintFunc()
var green = color.New(color.FgGreen).SprintFunc()
var red = color.New(color.FgRed).SprintFunc()
var blue = color.New(color.FgBlue).SprintFunc()

func toString(x interface{}) string {
	switch x.(type) {
	case string:
		return x.(string)
	case int:
		return strconv.Itoa(x.(int))
	case uint:
		return strconv.FormatUint(x.(uint64), 10)
	case float32:
		return strconv.FormatFloat(float64(x.(float32)), 'e', -1, 32)
	case float64:
		return strconv.FormatFloat(x.(float64), 'e', -1, 64)
	case bool:
		return strconv.FormatBool(x.(bool))
	}
	return ""
}
