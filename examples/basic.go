package main

import (
	"fmt"

	"bitbucket.org/ramankr/inquirer"
)

func main() {
	a, e := inquirer.Prompt([]*inquirer.Question{
		&inquirer.Question{
			Kind:    inquirer.Input,
			Name:    "name",
			Message: "What is your name",
		},
		&inquirer.Question{
			Kind:    inquirer.Password,
			Name:    "quest",
			Message: "What is your quest",
		},
		&inquirer.Question{
			Kind:    inquirer.List,
			Name:    "list",
			Message: "What is your favorite color",
			Choices: []*inquirer.ChoiceObject{
				&inquirer.ChoiceObject{
					Name:  "red",
					Value: "red",
				},
				&inquirer.ChoiceObject{
					Name:  "green",
					Value: "green",
				},
				&inquirer.ChoiceObject{
					Name:  "blue",
					Value: "blue",
				},
				&inquirer.ChoiceObject{
					Name:  "yellow",
					Value: "yellow",
				},
			},
		},
		&inquirer.Question{
			Kind:    inquirer.Checkbox,
			Name:    "speed",
			Message: "What is the air-speed velocity of a swallow",
			Choices: []*inquirer.ChoiceObject{
				&inquirer.ChoiceObject{
					Name:     "American?",
					Value:    "american",
					Disabled: true,
				},
				&inquirer.ChoiceObject{
					Name:  "African?",
					Value: "african",
				},
				&inquirer.ChoiceObject{
					Name:  "Europian?",
					Value: "europian",
				},
			},
		},
	})
	if e != nil {
		panic(e)
	}
	fmt.Println(a)
}
