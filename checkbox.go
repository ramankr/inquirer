package inquirer

import (
	"fmt"
	"runtime"

	"bytes"

	"github.com/chzyer/readline"
)

type CheckboxPrompt struct {
	idx            int
	choices        []*ChoiceObject
	enabledIndices []int
	questionMsg    string
}

const (
	checkboxPromptInitial int = iota
	checkboxPromptDisplaying
	checkboxPromptFinalizing
)

func (p *CheckboxPrompt) getPrompt(state int) string {
	b := bytes.NewBuffer([]byte{})

	if state == checkboxPromptInitial {
		b.WriteString(fmt.Sprintf("xx", len(p.choices)+1))
	}
	if state == checkboxPromptDisplaying {
		b.WriteString(fmt.Sprintf("\033[%dA", len(p.choices)+1))
	}
	if state == checkboxPromptFinalizing {
		b.WriteString(fmt.Sprintf("\033[%dA", len(p.choices)+2))
	}
	b.WriteString(fmt.Sprintf("[%s] %s\n", yellow("?"), p.questionMsg))

	for i, c := range p.choices {
		if c.Disabled {
			disabledMessage := c.DisabledMessage
			if disabledMessage == "" {
				disabledMessage = "disabled"
			}
			b.WriteString("    ")
			b.WriteString(gray(fmt.Sprintf("%s (%s)", c.Name, disabledMessage)))
		} else {
			if i == p.enabledIndices[p.idx] {
				b.WriteString(blue(" >"))
			} else {
				b.WriteString("  ")
			}
			if c.Checked {
				if runtime.GOOS == "windows" {
					b.WriteString("[x] ")
				} else {
					b.WriteString("◉ ")
				}
			} else {
				if runtime.GOOS == "windows" {
					b.WriteString("[ ] ")
				} else {
					b.WriteString("◯ ")
				}
			}
			b.WriteString(c.Name)
		}

		b.WriteRune('\n')
	}

	return b.String()
}

func (p *CheckboxPrompt) Ask(q *Question, answers map[string]Answer) error {
	p.questionMsg = q.getQuestion(answers)

	var defaultIndices []int
	p.choices, p.enabledIndices, defaultIndices = q.getOptionDetails(answers)
	if len(defaultIndices) > 0 {
		p.idx = defaultIndices[0]
	}

	fmt.Printf(p.getPrompt(checkboxPromptInitial))
	rl, err := readline.NewEx(&readline.Config{
		Prompt:                 p.getPrompt(checkboxPromptDisplaying),
		DisableAutoSaveHistory: true,
	})
	if err != nil {
		return err
	}

	rl.Config.SetListener(func(line []rune, pos int, key rune) (newLine []rune, newPos int, ok bool) {
		switch key {
		case readline.CharNext:
			p.idx++
			if p.idx >= len(p.enabledIndices) {
				p.idx = 0
			}
			rl.SetPrompt(p.getPrompt(checkboxPromptDisplaying))
			rl.Refresh()
		case readline.CharPrev:
			p.idx--
			if p.idx < 0 {
				p.idx = len(p.enabledIndices) - 1
			}
			rl.SetPrompt(p.getPrompt(checkboxPromptDisplaying))
			rl.Refresh()
		case ' ':
			p.choices[p.enabledIndices[p.idx]].Checked = !p.choices[p.enabledIndices[p.idx]].Checked
			rl.SetPrompt(p.getPrompt(checkboxPromptDisplaying))
			rl.Refresh()
		case readline.CharEnter:
			rl.SetPrompt(p.getPrompt(checkboxPromptFinalizing))
			rl.Refresh()
			return nil, 0, false
		default:
		}

		return nil, 0, true
	})

	_, err = rl.Readline()
	if err != nil {
		return err
	}

	rl.Close()
	var idxs = make([]interface{}, 0)
	for _, c := range p.choices {
		if c.Checked {
			idxs = append(idxs, c.Value)
		}
	}
	answers[q.Name] = idxs
	return nil
}
