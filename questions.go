package inquirer

import (
	"fmt"
	"reflect"
)

// MessageFunction should return the question to print
type MessageFunction func(answers map[string]Answer) string

// ValidateFunction should validate the input and return an error message if validation fails
type ValidateFunction func(input string) error

// DefaultFunction should return the default value for the question
type DefaultFunction func(answers map[string]Answer) error

// ChoicesFunction should return a []string or []*ChoiceObject
type ChoicesFunction func(answers map[string]Answer) error

// ChoiceObject represents a choice. Name is presented to the user and Value is stored in answers hash
type ChoiceObject struct {
	Name            string
	Value           interface{}
	Disabled        bool
	DisabledMessage string
	Checked         bool
}

// WhenFunction should return the default value for the question
type WhenFunction func(answers map[string]Answer) bool

// Question to be asked
type Question struct {
	Kind QuestionKind

	// Name to use in the answers hash.
	Name string

	// Default value(s) to use if nothing is entered. Must be a value or DefaultFunction
	Default interface{}

	// Message is the question to print. Must be a string or MessageFunction
	Message interface{}

	// Choices for list, rawlist and checkbox types. Must be a []string or []*ChoiceObject.
	Choices interface{}

	// Validate the user input
	Validate ValidateFunction

	// When indicates if the question should be asked. Should be bool or WhenFunction
	When interface{}
}

func (q *Question) validate() error {
	if q.Name == "" {
		return fmt.Errorf("Name must be provided")
	}

	switch q.Message.(type) {
	case MessageFunction:
	case string:
	default:
		return fmt.Errorf("Invalid message. Must be string or MessageFunction")
	}

	if q.Choices != nil {
		switch q.Choices.(type) {
		case []string:
		case []*ChoiceObject:
		default:
			return fmt.Errorf("Invalid choices. Must be []string or []*ChoiceObject")
		}
	}

	if q.When != nil {
		switch q.When.(type) {
		case WhenFunction:
		case bool:
		default:
			return fmt.Errorf("Invalid when. Must be bool or WhenFunction")
		}
	}

	return nil
}

func (q *Question) shouldAsk(answers map[string]Answer) bool {
	if q.When != nil {
		switch q.When.(type) {
		case bool:
			return q.When.(bool)
		case WhenFunction:
			return q.When.(WhenFunction)(answers)
		}
	}
	return true
}

func (q *Question) ask(answers map[string]Answer) error {
	switch q.Kind {
	case Input:
		return (&InputPrompt{}).Ask(q, answers)
	case Password:
		return (&InputPrompt{Password: true}).Ask(q, answers)
	case List:
		return (&ListPrompt{}).Ask(q, answers)
	case Checkbox:
		return (&CheckboxPrompt{}).Ask(q, answers)
	}
	return fmt.Errorf("Unknown prompt type")
}

func (q *Question) getQuestion(answers map[string]Answer) string {
	switch q.Message.(type) {
	case string:
		return q.Message.(string)
	case MessageFunction:
		return q.Message.(MessageFunction)(answers)
	}
	return ""
}

func (q *Question) getDefault(answers map[string]Answer) interface{} {
	switch q.Default.(type) {
	case DefaultFunction:
		return q.Default.(DefaultFunction)(answers)
	default:
		return q.Default
	}
}

func (q *Question) getOptionDetails(answers map[string]Answer) (choices []*ChoiceObject, enabledIndices []int, defaultIndices []int) {
	switch q.Choices.(type) {
	case []string:
		choices = make([]*ChoiceObject, len(q.Choices.([]string)))
		for i, c := range q.Choices.([]string) {
			choices[i] = &ChoiceObject{
				Name:  c,
				Value: c,
			}
		}
	case []*ChoiceObject:
		choices = q.Choices.([]*ChoiceObject)
	}

	defaultValue := q.getDefault(answers)
	defaultValueArr := make([]interface{}, 0)
	if defaultValue != nil {
		s := reflect.ValueOf(defaultValue)
		if s.Kind() != reflect.Slice {
			defaultValueArr = append(defaultValueArr, defaultValue)
		}
	}

	enabledIndices = make([]int, 0)
	defaultIndices = make([]int, 0)
	for i, v := range choices {
		if !v.Disabled {
			enabledIndices = append(enabledIndices, i)
		}
		for _, dval := range defaultValueArr {
			if i == dval || v.Value == dval {
				defaultIndices = append(defaultIndices, len(enabledIndices)-1)
			}
		}
	}
	return
}
