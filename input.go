package inquirer

import (
	"fmt"

	"github.com/chzyer/readline"
)

type InputPrompt struct {
	Password bool
}

func (i *InputPrompt) Ask(q *Question, answers map[string]Answer) error {
	defaultValue := q.getDefault(answers)

	prompt := fmt.Sprintf("[%s] %s: ", yellow("?"), q.getQuestion(answers))
	if defaultValue != nil {
		prompt = fmt.Sprintf("[%s] %s [%s]: ", yellow("?"), q.getQuestion(answers), green(toString(defaultValue)))
		answers[q.Name] = defaultValue
	}

	rl, err := readline.NewEx(&readline.Config{
		Prompt:                 prompt,
		DisableAutoSaveHistory: true,
	})

	if i.Password {
		rl.Config.MaskRune = '*'
		rl.Config.EnableMask = true
	}
	if err != nil {
		return err
	}
	for {
		val, err := rl.Readline()
		if err != nil {
			return err
		}

		if val != "" {
			if q.Validate != nil {
				if verr := q.Validate(val); verr != nil {
					rl.Terminal.Print(fmt.Sprintf("%s %s\n", red(">>"), verr.Error()))
					continue
				}
			}
			rl.Close()
			answers[q.Name] = val
			break
		}
	}

	return nil
}
