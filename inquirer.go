package inquirer

type QuestionKind int

const (
	Input QuestionKind = iota
	Password
	List
	Checkbox
)

// Answer from the user
type Answer interface{}

// Prompt starts asking the user questions
func Prompt(questions []*Question) (map[string]Answer, error) {
	for _, q := range questions {
		if err := q.validate(); err != nil {
			return nil, err
		}
	}

	answers := make(map[string]Answer)
	for _, q := range questions {
		if q.shouldAsk(answers) {
			if err := q.ask(answers); err != nil {
				return nil, err
			}
		}
	}

	return answers, nil
}
